#!/bin/bash
sudo apt-get install python3-pip python3-dev python-virtualenv python3-tk # for Python 3.n
virtualenv --system-site-packages -p python3 virt_py
source virt_py/bin/activate
pip install -r requirements.txt
#pip3 install --upgrade tensorflow
#pip3 install scikit-learn

