'''
Created on Oct 16, 2017

@author: amir
'''
import unittest
import numpy as np
from OutDet import NetworkTrainer
from OutDet.NetworkBuilder2 import NetworkBuilder


def mock_data_gen():
    while True:
        yield(np.zeros((100,10)))

class Test(unittest.TestCase):


    def testGeneratorFunction(self):
        trainer = NetworkTrainer.NetworkOutlierDetector(NetworkBuilder(10),batchSize = 100)
        trainer.fit(mock_data_gen(),justInit_Dont_train=True)
#         assert trainer.
    
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testGeneratorFunction']
    unittest.main()