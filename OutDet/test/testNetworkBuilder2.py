'''
Created on May 2, 2017

@author: amir
'''
import unittest

import OutDet.NetworkBuilder2 as nb
import numpy as np
import tensorflow as tf

class MyGen:
    def __init__(self,data):
        self.data = data
    def generate(self,a,b):
        return self.data


class Test(tf.test.TestCase):

    def setUp(self):
        #self.sep_points = [[1,-2,3],[-3,-4,5],[-7,-8,9],[10,-11,12]]
        pass

    def tearDown(self):
        pass

    def testInternals(self):
        g = tf.Graph()
        with g.as_default():
            with self.test_session():
                raw_inputs = [[1,2,2], [-3,-6,-3]]
                sep_points = [[0.5,0,0],[0,0,0],[0,0,0],[0,0,0]]
                sepGen = MyGen(sep_points)
                nUnits = 4
                
                builder = nb.NetworkBuilder(nUnits, seperation_points_gen=sepGen, std_normalize=False)
                
                inputs = tf.constant(raw_inputs,dtype=tf.float32)
                res = builder.build(inputs,raw_inputs)["midpoints"]
                
                with tf.variable_scope("", reuse=True):
                    bias_ones = [0,1,5,100]
                    biases = tf.get_variable("fully_connected/biases",)
                    biases.assign(bias_ones).eval()
                    
                    weight_ones = [[1,2,5,1],[1,0,-1,-5],[-1,3,4,2]]
                    weights = tf.get_variable("fully_connected/weights",)
                    weights.assign(weight_ones ).eval()
                
                #norm = res['normed']
                
                randoffset = res['randoffset']
                expectedRO = np.dot(sepGen.generate(1,1),weight_ones).diagonal()
                self.assertAllClose(expectedRO, randoffset.eval())
                
                fc = res['fc']
                predictions = res['predictions']
                
                loss = res['loss']
                
#                 input_std = np.std(raw_inputs, axis=1, keepdims=True)
#                 input_mean = np.mean(raw_inputs, axis=1, keepdims=True)
#                 expNorm = (raw_inputs-input_mean)/input_std
#                 self.assertAllClose(norm.eval(), expNorm )
                
                expNorm = np.asarray(raw_inputs)
                
                max_row = 2
                max_col = 4
                ndim = 3
                expfc = np.zeros([max_row,max_col])
                for i in range (max_row):
                    for j in range (max_col):
                        for k in range(ndim):
                            wt = weight_ones[k][j]
                            sp = sep_points[j][k]
                            expfc[i,j] += (expNorm[i][k]-sp)*wt  
                        expfc[i,j] += bias_ones[j]
                #expfc = np.add(np.dot(expNorm,weight_ones),bias_ones)
                
                self.assertAllClose(expfc, fc.eval())
                
                exppred = np.max(expfc, axis=1)
                self.assertAllClose(exppred, predictions.eval())
                
                
                expLoss = np.sum(exppred)
                self.assertTrue(len (loss.get_shape()) == 0 )
                self.assertAllClose(expLoss, loss.eval())


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()