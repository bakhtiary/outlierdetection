'''
Created on Nov 30, 2016

@author: amir
'''
import unittest
import tensorflow as tf
import OutDet.NetworkBuilder as nb
import numpy as np

class TestSimpleBuilder(tf.test.TestCase):


    def setUp(self):
        self.builder = nb.SimpleBuilder(4)
        pass


    def tearDown(self):
        pass


    def testInternals(self):
        g = tf.Graph()
        with g.as_default():
            with self.test_session():
                raw_inputs = [[1,2,2], [-3,-6,-3]]
                inputs = tf.constant(raw_inputs,dtype=tf.float32)
                res = self.builder.build(inputs)["midpoints"]
                
                with tf.variable_scope("", reuse=True):
                    bias_ones = [1,1,1,1]
                    biases = tf.get_variable("fully_connected/biases",)
                    biases.assign(bias_ones).eval()
                    
                    weight_ones = [[1,1,1,1],[1,1,1,1],[1,1,1,1]]
                    weights = tf.get_variable("fully_connected/weights",)
                    weights.assign(weight_ones ).eval()
                
                print (res)
                    
                #norm = res[0]
                fc = res[0]
                loss = res[1]

                expNorm = raw_inputs#/np.linalg.norm(raw_inputs,axis=1,keepdims=True)
                #self.assertAllClose(norm.eval(), expNorm )
                                 
                expfc = np.clip(np.matmul(expNorm, weight_ones ) + bias_ones,0,100000)
                self.assertAllClose(expfc, fc.eval())
                
                expLoss = np.sum(expfc)
                #self.assertTrue(len (loss.get_shape()) == 1 )
                self.assertAllClose(expLoss, loss.eval())

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()