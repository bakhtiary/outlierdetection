'''
Created on May 5, 2017

@author: amir
'''
import unittest
from OutDet.OriginGenerators import Aggregator
import numpy as np

class ReturnZeros:
    def randint(self,min,max,shape):
        return np.zeros(shape,dtype=np.int)

class ReturnZerosOnesAlternate:
    def randint(self,min,max,shape):
        retval = np.zeros(shape,dtype=np.int)
        retval[::2] = 1
        return retval
        
class ReturnAllRepeating:
    def randint(self,min,max,shape):
        retval = np.asarray(range(shape),dtype=np.int)
        retval = np.mod(retval,max)
        return retval

class TestAggregator(unittest.TestCase):

    def testPickSingle(self):
        zeros = ReturnZeros();
        
        ag = Aggregator(2,0.1,zeros)
        nUnits = 5;
        
        data = np.asarray([[1,2,3],[1,2,3],[1,2,3],[1,1,1]])
        generated = ag.generate(data, nUnits)
        
        expected = np.asarray([[1,2,3]]*nUnits)*0.1
        np.testing.assert_allclose(generated, expected)
        
    def testAlternate(self):
        alternate = ReturnZerosOnesAlternate();
        
        ag = Aggregator(2,0.1,alternate)
        nUnits = 5;
        
        data = np.asarray([[1,2,3],[0,1,2],[1,2,3],[1,1,1]])
        generated = ag.generate(data, nUnits)
        
        expected = np.asarray([[0.5,1.5,2.5]]*nUnits)*0.1
        np.testing.assert_allclose(generated, expected)

    def testDataIterator(self):
        
        def generator():
            i = 0
            while True:
                ones = np.ones([3,4], np.float32)
                yield( ones*i)
                i+=1
        
        ag = Aggregator(2,0.1,ReturnZerosOnesAlternate())
        generated = ag.generate(generator(), 4 )
        
        expected = np.asarray([[ 0.1, 0.1],
                                 [ 0.1, 0.1],
                                 [ 0.15, 0.15],
                                 [ 0.15,  0.15]
                                 ]
                              )
        np.testing.assert_allclose(generated, expected)

        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testAggregator']
    unittest.main()