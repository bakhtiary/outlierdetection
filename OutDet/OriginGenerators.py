'''
Created on May 5, 2017

@author: amir
'''

import numpy as np
import collections

def standardize(x):
    mean = np.mean(x,axis=1,keepdims=True)
    std = np.std(x,axis=1,keepdims=True)
    return x/std - mean/std


class Aggregator(object):
    '''
    classdocs
    '''


    def __init__(self, nfolds=10, scale=1, randGen=np.random.RandomState(12345678)):
        '''
        Constructor
        '''
        self.nfolds = nfolds
        self.scale = scale
        self.randGen = randGen
        
    def generate(self,data,nUnits,standardize_if_working_with_iterator = False):
        if hasattr(data, "get_next_batch"):
            nBatch = data.get_next_batch().shape[0]
            n_needed = nUnits*self.nfolds
            arrs = []
            for i in range(int(n_needed/nBatch)+1):
                arrs.append(data.get_next_batch())
            
            new_data = np.concatenate(arrs)[:n_needed]
            if standardize_if_working_with_iterator :
                new_data = standardize (new_data)
        else:
            nmax = data.shape[0]
            new_data = data[self.randGen.randint(0,nmax,nUnits*self.nfolds),:]
            
        return np.mean(new_data.reshape(self.nfolds,nUnits,-1),axis=0)*self.scale
    
    def __repr__(self):
        return "ag_%s_%s"%(self.nfolds,self.scale)   
    
    def __str__(self):
        return "ag_%s_%s"%(self.nfolds,self.scale)
