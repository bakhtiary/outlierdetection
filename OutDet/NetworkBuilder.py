'''
Created on Nov 30, 2016

@author: amir
'''
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'


import tensorflow as tf
from tensorflow.contrib import slim
from tensorflow.python.framework import ops
from tensorflow.python.ops import nn
from tensorflow.python.ops import variable_scope
from tensorflow.python.ops import standard_ops
from tensorflow.contrib.layers.python.layers import utils
from tensorflow.contrib.framework.python.ops import variables


def make_image_summary(name,var):
    
    var_expanded = var 
    while len (var_expanded.get_shape()) < 3:
        var_expanded = tf.expand_dims(var_expanded, 0)
    var_expanded = tf.expand_dims(var_expanded, -1)
    tf.summary.image(name, var_expanded,100)

class SimpleBuilder(object):
    
    def __init__(self, nInternalUnits):
        self.nInternalUnits = nInternalUnits
        pass

    def getSpecs(self):
        return "nUnits_%s"%(
            self.nInternalUnits,
        )

    def build(self,inputs,regularization_points = None):
        
        inputs_shape = inputs.get_shape()
        inputs_rank = inputs_shape.ndims
        fc = slim.fully_connected(inputs, num_outputs = self.nInternalUnits, weights_regularizer= slim.regularizers.l2_regularizer(0.1))
        
        predictions = tf.reduce_sum(fc,1)
        loss = tf.reduce_sum(fc)
        tf.losses.add_loss(loss)
        
        total_loss = tf.losses.get_total_loss()
        
        tf.summary.scalar('loss', loss)
        tf.summary.histogram("fc", fc )
        #tf.summary.histogram("biases", slim.get_model_variables()[0] )
        tf.summary.histogram("weights", slim.get_model_variables()[0] )
            
        merged_summaries = tf.summary.merge_all()
        
        return {"midpoints":[ fc, loss],
                "loss":loss,
                "totalLoss":total_loss,
                "predictions":predictions,
                "weights":slim.get_model_variables(),
                "merged_summaries":merged_summaries,
                 }

class SimpleBuilder_TwoLayers(object):
    
    def __init__(self, nInternalUnits):
        self.nInternalUnits = nInternalUnits
        pass

    def build(self,inputs):
        
#         normed = slim.layers.layer_norm(inputs,center=False,scale=False,)
        fc1 = slim.fully_connected(inputs, num_outputs = self.nInternalUnits, weights_regularizer= slim.regularizers.l2_regularizer(0.1))
        fc2 = slim.fully_connected(fc1, num_outputs = self.nInternalUnits, weights_regularizer= slim.regularizers.l2_regularizer(0.1))
#        fc2 = slim.fully_connected(fc1, num_outputs = self.nInternalUnits, trainable = False)
        
        predictions = tf.reduce_sum(fc2,1,name="predictions")
        loss = tf.reduce_sum(fc2,name="loss")
        slim.losses.add_loss(loss)
        
        total_loss = tf.contrib.losses.get_total_loss()
        
        tf.summary.scalar('loss', loss)
        tf.summary.histogram("fc1", fc1 )
        tf.summary.histogram("fc2", fc2 )
        #tf.summary.histogram("biases", slim.get_model_variables()[0] )
        tf.summary.histogram("weights", slim.get_model_variables()[0] )
         
#         tf.summary.scalar('loss', loss)
#         make_image_summary("fc2", fc2)
#         make_image_summary("fc1", fc1)

#         make_image_summary("var0", slim.get_model_variables()[0] )
#         make_image_summary("var1", slim.get_model_variables()[1] )
#         make_image_summary("var2", slim.get_model_variables()[2] )
#         make_image_summary("var2", slim.get_model_variables()[3] )
#       
        
            
        merged_summaries = tf.summary.merge_all()
        
        return {"midpoints":[fc1, fc2, predictions, loss],
                "weights":slim.get_model_variables(),
                "loss":loss,
                "totalLoss":total_loss,
                "predictions":predictions,
                "weights":slim.get_model_variables(),
                "merged_summaries":merged_summaries,
                 }


def l2_regularizer_random(scale, scope=None, stddev = 1):
  """Returns a function that can be used to apply L2 regularization to weights.

  Small values of L2 can help prevent overfitting the training data.

  Args:
    scale: A scalar multiplier `Tensor`. 0.0 disables the regularizer.
    scope: An optional scope name.

  Returns:
    A function with signature `l2(weights)` that applies L2 regularization.

  Raises:
    ValueError: If scale is negative or if scale is not a float.
  """
    
  def l2(weights):
    """Applies l2 regularization to weights."""
    with ops.name_scope(scope, 'l2_regularizer', [weights]) as name:
      my_scale = ops.convert_to_tensor(scale,
                                       dtype=weights.dtype.base_dtype,
                                       name='scale')
      regPoint = ops.convert_to_tensor(tf.random_normal(weights.get_shape(),stddev = stddev),                                       
                                       dtype=weights.dtype.base_dtype,
                                       name='regPoint')
      
      return standard_ops.multiply(my_scale, nn.l2_loss(tf.subtract(weights,regPoint)), name=name)

  return l2



class SimpleBuilder_AddAOne(object):
    
    def __init__(self, nInternalUnits ):
        self.nInternalUnits = nInternalUnits
        pass

    def build(self,inputs):
#         normed = slim.layers.layer_norm(inputs,center=False,scale=False,)
        inputs_shape = inputs.get_shape()
        batch_size = inputs_shape[0] 
        inputs_rank = inputs_shape.ndims
        
        ones = tf.expand_dims(tf.constant([1.0] * batch_size), 1)
        concatedWithOnes = tf.concat(1, [inputs, ones],"concatedWithOnes")

        axis = list(range(1, inputs_rank))
        normed = nn.l2_normalize(concatedWithOnes, axis, name="normed")
        
        #fc = slim.fully_connected(normed, num_outputs = self.nInternalUnits, weights_regularizer= slim.regularizers.l2_regularizer(1.0), normalizer_fn=lambda x: x )
        weights_shape = [concatedWithOnes.get_shape()[1], self.nInternalUnits]
        weights_collections = utils.get_variable_collections(None, 'weights')
        weights = variables.model_variable('weights',
                                           shape=weights_shape,
#                                           dtype=dtype,
#                                           initializer=weights_initializer,
                                           regularizer=l2_regularizer_random(0.2, stddev=2),
                                           collections=weights_collections,
                                           trainable=True)
        
        fc = nn.relu ( standard_ops.add( standard_ops.matmul(normed, weights ) , 10.0 ) )
        predictions = tf.reduce_max(fc,1)
        loss = tf.reduce_max(fc)
        slim.losses.add_loss(loss)
        
        total_loss = tf.contrib.losses.get_total_loss()
        
        tf.summary.scalar('loss', loss)
        tf.summary.histogram("normed", normed )
        tf.summary.histogram("fc", fc )
        #tf.summary.histogram("biases", slim.get_model_variables()[0] )
        tf.summary.histogram("weights", slim.get_model_variables()[0] )

            
        merged_summaries = tf.summary.merge_all()
        
        return {"midpoints":[normed, concatedWithOnes, fc, loss],
                "loss":loss,
                "totalLoss":total_loss,
                "concatedWithOnes":concatedWithOnes,
                "predictions":predictions,
                "weights":slim.get_model_variables(),
                "merged_summaries":merged_summaries,
                 }
        


class TwoLayers_advanced(object):
    
    def __init__(self, nInternalUnits , nInternalUnits2 ):
        self.nInternalUnits = nInternalUnits
        self.nInternalUnits2 = nInternalUnits2
        pass

    
    def build(self,inputs):
#         normed = slim.layers.layer_norm(inputs,center=False,scale=False,)
        inputs_shape = inputs.get_shape()
        batch_size = inputs_shape[0] 
        inputs_rank = inputs_shape.ndims
        
        ones = tf.expand_dims(tf.constant([1.0] * batch_size), 1)
        concatedWithOnes = tf.concat(1, [inputs, ones],"concatedWithOnes")

        axis = list(range(1, inputs_rank))
        normed = nn.l2_normalize(concatedWithOnes, axis, name="normed")
        
        weights_shape = [concatedWithOnes.get_shape()[1], self.nInternalUnits]
        weights_collections = utils.get_variable_collections(None, 'weights')
        weights = variables.model_variable('weights1',
                                           shape=weights_shape,
#                                           dtype=dtype,
#                                           initializer=weights_initializer,
#                                             regularizer=slim.regularizers.l2_regularizer(0.1),
                                            regularizer=l2_regularizer_random(0.2, mean = 5, stddev=1),
                                            collections=weights_collections,
                                            trainable=True)
        
        fc1 = nn.relu ( standard_ops.add( standard_ops.matmul(normed, weights ) , 0.0 ) )
        

        weights_shape = [self.nInternalUnits,self.nInternalUnits2]
        weights2 = variables.model_variable('weights2',
                                           shape=weights_shape,
#                                           dtype=dtype,
#                                           initializer=weights_initializer,
#                                            regularizer=slim.regularizers.l2_regularizer(0.1),
                                           regularizer=l2_regularizer_random(0.2, mean = 5, stddev=1),
                                           collections=weights_collections,
                                           trainable=True)
 
        fc2 = nn.relu ( standard_ops.add( standard_ops.matmul(fc1, weights2 ) , 0.0 ) )
         
        predictions = tf.reduce_max(fc2,1)
        loss = tf.reduce_max(fc2)
        slim.losses.add_loss(loss)
        
        total_loss = tf.contrib.losses.get_total_loss()
        
        tf.summary.scalar('loss', loss)
        tf.summary.histogram("normed", normed )
        tf.summary.histogram("fc", fc1 )
        #tf.summary.histogram("biases", slim.get_model_variables()[0] )
        tf.summary.histogram("weights", slim.get_model_variables()[0] )

            
        merged_summaries = tf.summary.merge_all()
        
        return {"midpoints":[normed, concatedWithOnes, fc1, fc2, loss],
                "loss":loss,
                "totalLoss":total_loss,
                "concatedWithOnes":concatedWithOnes,
                "predictions":predictions,
                "weights":slim.get_model_variables(),
                "merged_summaries":merged_summaries,
                 }
        
        
