'''
Created on Nov 30, 2016

@author: amir
'''

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import numpy as np

import tensorflow as tf
from tensorflow.contrib import slim
from tensorflow.python.framework import ops
from tensorflow.python.ops import nn
from tensorflow.python.ops import variable_scope
from tensorflow.python.ops import standard_ops
from tensorflow.contrib.layers.python.layers import utils
from tensorflow.contrib.framework.python.ops import variables

import collections


def dec_regularizer(scale, scope=None):
    
    if scale == 0.:
        return lambda _: None

    def dec(weights, name=None):
        with ops.name_scope(scope, 'dec_regularizer', [weights]) as name:
            my_scale = ops.convert_to_tensor(scale,
                                             dtype=weights.dtype.base_dtype,
                                             name='scale')
            return standard_ops.multiply(
                -my_scale,
                standard_ops.reduce_sum(weights),
                name=name)

    return dec

def standardize(x):
    mean = np.mean(x,axis=1,keepdims=True)
    std = np.std(x,axis=1,keepdims=True)
    return x/std - mean/std


class NetworkBuilder(object):
    
    def __init__(self, nInternalUnits, weight_reg_scale = 0.1, biases_reg_scale = 0.1,
                weight_init_stddev = 0.1,
                softmaxBased = False,
                bias_lr_mult = 1.0,
                bias_initializer = 1.0, 
                dropout_keep_prob = 1.0,
                dropout_keep_prob2 = 1.0,
                random_weight_reg = 0.0,
                std_normalize = True,
                seperation_points_gen = None,
                append_to_name = "",
                ):
        self.nInternalUnits = nInternalUnits
        self.weight_reg_scale = weight_reg_scale
        self.weight_init_stddev = weight_init_stddev
        self.biases_reg_scale = biases_reg_scale
        self.softmaxBased = softmaxBased
        self.bias_lr_mult = bias_lr_mult 
        self.bias_initializer = bias_initializer
        self.dropout_keep_prob = dropout_keep_prob
        self.dropout_keep_prob2 = dropout_keep_prob2
        self.random_weight_reg = random_weight_reg
        self.std_normalize = std_normalize
        self.seperation_points_gen = seperation_points_gen
        self.append_to_name = append_to_name
        pass

    def getSpecs(self):
        return "Uns_%s_wrs_%s_brs_%s_wis_%s_bi_%s_sof_%s_blr_%s_dkp_%s_%s_rwr_%s_sep_%s_%s"%(
            self.nInternalUnits, self.weight_reg_scale,
            self.biases_reg_scale,
            self.weight_init_stddev,
            self.bias_initializer,
            self.softmaxBased,self.bias_lr_mult,
            self.dropout_keep_prob,
            self.dropout_keep_prob2,
            self.random_weight_reg,
            self.seperation_points_gen,
            self.append_to_name,
        )
    
    def getGradMultChanges(self):
        return { "fully_connected/biases":self.bias_lr_mult }
    
    def build(self,inputs,all_data):
        midpoints = {}
        midpoints['inputs'] = inputs
                
        if self.std_normalize:
            input_mean, input_variance = nn.moments(inputs, [1], keep_dims=True)
            normed = tf.divide(tf.subtract(inputs,input_mean),tf.sqrt(input_variance))
            if not hasattr(all_data, "get_next_batch") :
                all_data = standardize(all_data)
        else:
            normed = inputs
        
        midpoints['normed'] = normed
        
        #normed = inputs
        
#        keep_prob = tf.Variable(self.dropout_keep_prob) #dropout (keep probability)
#        normed = tf.nn.dropout(normed, keep_prob)
        
        inputs_shape = inputs.get_shape()
        batch_size = inputs_shape[0]
        inputs_rank = inputs_shape.ndims
        normalizer_fn = None      
        
        fc = slim.fully_connected(
            normed,
            num_outputs = self.nInternalUnits,
            weights_regularizer = slim.regularizers.l2_regularizer(self.weight_reg_scale/self.nInternalUnits),
            weights_initializer = tf.random_normal_initializer(stddev=self.weight_init_stddev), 
                                    #slim.initializers.xavier_initializer(),
            normalizer_fn = normalizer_fn,
            activation_fn = None,
            biases_regularizer = dec_regularizer(self.biases_reg_scale/self.nInternalUnits),
            biases_initializer = tf.constant_initializer(self.bias_initializer)
        )
        
#        keep_prob2 = tf.Variable(self.dropout_keep_prob2) #dropout (keep probability)
#        fc = tf.nn.dropout(fc, keep_prob2)
        
        if self.random_weight_reg != 0.0 :
            weights = slim.get_model_variables()[0]
            init_weights = tf.Variable(weights.initialized_value())
            w_reg = tf.nn.l2_loss(tf.scalar_mul( 
                                                self.random_weight_reg,
                                                tf.subtract(weights,init_weights),
                                               ) 
                                 )
#            w_reg = tf.reduce_sum ( tf.abs( tf.scalar_mul( 
#                                                 self.random_weight_reg,
#                                                 tf.subtract(weights,init_weights),
#                                    ))) 
            tf.losses.add_loss(w_reg)
            tf.summary.scalar('w_reg', w_reg)

        if self.seperation_points_gen is not None:
            
            random_bias = tf.constant(
                self.seperation_points_gen.generate(all_data,self.nInternalUnits,self.std_normalize),
                dtype = tf.float32,
                )
            #print (random_bias.shape)
            weights = slim.get_model_variables()[0]
            
            randoffset = tf.reduce_sum(tf.multiply(weights, tf.transpose(random_bias) ),axis = 0)
            midpoints ['randoffset'] = randoffset
            #print randoffset.shape
            fc = tf.add(fc,tf.scalar_mul(-1.0,randoffset) )
        
        midpoints['fc'] = fc
        
        zeros = tf.expand_dims(tf.constant([0.0] * batch_size), 1,name="zerosForConcat")
        fc_concated_with_zero = tf.concat([zeros,fc], 1,name="concatedWithzeros")
        midpoints['fc_concated_with_zero'] = fc_concated_with_zero 
            
        if self.softmaxBased :
            softmax = tf.nn.softmax(fc_concated_with_zero,-1,name = "softmax")
            predictions = standard_ops.multiply(
                -1.0,softmax[:,0],name = "predictions")
            
            labels = tf.one_hot([0]*batch_size,fc_concated_with_zero.shape[1],name="labels")
            partialLoss = tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=fc_concated_with_zero,name="partialLoss")
            midpoints ['partialLoss'] = partialLoss
            loss = tf.reduce_mean(partialLoss,name="total_loss")
        else:
            predictions = tf.reduce_max(fc,1)
            predictions_for_loss = tf.reduce_max(fc_concated_with_zero,1)
            loss = tf.reduce_mean(predictions_for_loss)

        midpoints['predictions'] = predictions
        midpoints['loss'] = loss
         
        tf.losses.add_loss(loss)
        total_loss = tf.losses.get_total_loss()
        midpoints['total_loss'] = total_loss
        
        tf.summary.scalar('loss', loss)
        tf.summary.scalar('total_loss', total_loss)
        tf.summary.histogram("fc", fc )
        if (len (slim.get_model_variables()) == 2):
            tf.summary.histogram("biases", slim.get_model_variables()[1] )
        tf.summary.histogram("weights", slim.get_model_variables()[0] )
        
        return {"midpoints": midpoints,
                "loss":loss,
                "totalLoss":total_loss,
                "predictions":predictions,
                "weights":slim.get_model_variables(),
                 }

            

