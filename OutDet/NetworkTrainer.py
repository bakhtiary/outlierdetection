import pickle
import tensorflow as tf
import numpy as np
import os
from sklearn.metrics import roc_auc_score
import inspect
import collections


class NetworkOutlierDetector(object):

    def __init__(self, networkBuilder, batchSize = 100,  nIterations = 1, nStatIters = 1, learning_rate = 0.001,summaries_dir = "/tmp/log", log_everything_as_nparray = False, save_summary_every_n = 0,
                 perform_test_every_n = 10,
                 testDataset = None,
                 testlossDataset = None,
                 verboseOutput = False,
                 lr_decay_steps = 1000,
                 lr_decay_rate = 0.1,
                 lr_momentum = 0.9,
                 gpu_ram_limit = 0.1,
                 use_cpu_device = False,
                 justInit_Dont_train = False,
                ):
        self.batchSize = batchSize
        self.nIterations = nIterations
        self.nStatIters = nStatIters
        self.builder= networkBuilder
        self.summaries_dir = summaries_dir
        self.log_everything = log_everything_as_nparray
        self.learning_rate = learning_rate
        self.save_summary_every_n = save_summary_every_n
        self.testDataset = testDataset
        self.perform_test_every_n = perform_test_every_n
        self.verboseOutput = verboseOutput
        self.decay_steps = lr_decay_steps
        self.decay_rate = lr_decay_rate
        self.momentum = lr_momentum
        self.testlossDataset = testlossDataset
        self.gpu_ram_limit = gpu_ram_limit
        self.use_cpu_device = use_cpu_device
        self.justInit_Dont_train = justInit_Dont_train
        
        i = 0
        while True:
            nextDir = "%s/%03d" %(self.summaries_dir, i)
            if not os.path.exists( nextDir ):
                break
            i += 1
        
        if self.verboseOutput:
            print ("run Number = " + str(i) )

        self.summaries_dir = nextDir + "/" + networkBuilder.getSpecs() + "_bs_%d_lr_%s_mom_%s"%(
                                                            batchSize,learning_rate,lr_momentum)
        if self.verboseOutput:
            print ("saving to "+self.summaries_dir)
        
        self.curStep = 0
        
        self.g = None
        pass

    def fit(self, data):
        
        if self.g is None:
            self.g = tf.Graph()
            with self.g.as_default():
                if self.use_cpu_device:
                    device_string = '/cpu:0'
                else:
                    device_string = '/gpu:0'
                    
                with tf.device(device_string):
                    tf.set_random_seed(12345)
                    if self.verboseOutput:
                        print ("initializing network")
                    if hasattr(data, "get_next_batch"):
                        self.data_shape = data.get_next_batch().shape
                        data_dim = self.data_shape[1]
                        assert (self.data_shape[0] == self.batchSize), "the number of samples in generator and the batch size are not equal"
                    else:
                        self.data_shape = data.shape
                        data_dim = self.data_shape[1]
                    
                    self.data_in = tf.placeholder(
                        tf.float32, (self.batchSize, data_dim))

                    self.network = self.builder.build(self.data_in, data)
                    self.predictions = self.network["predictions"]
                    self.loss = self.network["totalLoss"]
                    self.weights = self.network["weights"]

                    #opt = tf.train.MomentumOptimizer(
                    #    learning_rate=self.learning_rate,momentum=0.9, use_locking=False, name='GradientDescent', use_nesterov=True)
                    #opt = tf.train.GradientDescentOptimizer(self.learning_rate)

                    self.global_step = tf.placeholder(tf.int32)
                    
                    self.learning_rate_var = tf.Variable( self.learning_rate,name="learning_rate_var")
                    
                    rl_exp = tf.train.exponential_decay(self.learning_rate_var, global_step = self.global_step, 
                                               decay_steps = self.decay_steps,
                                               decay_rate = self.decay_rate,         
                                               staircase=True, name=None)
                    
                    self.rl_exp = rl_exp

                    tf.summary.scalar('learning_rate', rl_exp)

                    opt = tf.train.MomentumOptimizer(rl_exp, momentum=self.momentum, use_nesterov=True)

                    grads_and_vars = opt.compute_gradients(self.loss)
                    if (hasattr( self.builder, 'getGradMultChanges') and inspect.ismethod(self.builder.getGradMultChanges)):
                        changeMult = self.builder.getGradMultChanges()
                    else:
                        changeMult = {}
                    grads_and_vars_mult = []
                    for grad, var in grads_and_vars:
                        if (var.op.name in changeMult):
                            grad *= changeMult[var.op.name]
                        grads_and_vars_mult.append((grad, var))

                    self.opt_op = opt.apply_gradients(grads_and_vars_mult)


                    #self.opt_op = opt.minimize(self.loss, var_list=self.weights)

                    self.merged_summaries = tf.summary.merge_all()

                    self.rocscoreVar = tf.Variable(0.0)
                    self.test_loss_var = tf.Variable(0.0)
                    self.rocscoreSummary = tf.summary.scalar('rocscore', self.rocscoreVar)
                    self.test_lossSummary = tf.summary.scalar('test_loss', self.test_loss_var)

                    
                    config = tf.ConfigProto(
                        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=self.gpu_ram_limit),
                        allow_soft_placement=True,
#                        device_count = {'GPU': 1}
                    )
                    
                    self.sess = tf.Session(
                        config=config
                    )
                    if  self.save_summary_every_n != 0 or self.log_everything:
                        self.writer = tf.summary.FileWriter(self.summaries_dir,
                                              self.sess.graph)
                    self.sess.run(tf.global_variables_initializer())

            self.last_data = data
            if not self.justInit_Dont_train:
                with self.g.as_default():
                    self.train(data)
#                    self.getStats(data)
            
            
    def train(self, data = None):
        if data == None:
            data = self.last_data
        else:
            self.last_data = data
            
        nSamples = self.data_shape[0]
        for i in range(self.nIterations):
            curLoc = (self.curStep*self.batchSize)%(nSamples-self.batchSize+1)
            
            if self.testDataset is not None and self.curStep%self.perform_test_every_n == 0:
                pred = self.decision_function(self.testDataset[0])
                score = roc_auc_score(self.testDataset[1],pred)
                rocsumm = self.sess.run(self.rocscoreSummary,{self.rocscoreVar:score} )
                if self.save_summary_every_n != 0:
                    self.writer.add_summary(rocsumm, self.curStep)
                if self.verboseOutput:
                    print ("roc_: "+str(score) )
            
            if self.testlossDataset is not None and self.curStep%self.perform_test_every_n == 0:
#                print "running test loss"
                test_loss = 0.0
                count = 0
                for testI in range (0,len(self.testlossDataset)-self.batchSize,self.batchSize) :
#                    print (testI)
                    loss_val = self.sess.run(self.loss,
                                         {self.data_in:self.testlossDataset[testI:testI+self.batchSize,:],
                                          self.global_step:self.curStep}
                                        )
                    test_loss += loss_val
                    count+=1
                test_loss/=count
                if self.save_summary_every_n != 0:
                    tlsumm = self.sess.run(self.test_lossSummary,{self.test_loss_var:test_loss} )
                    self.writer.add_summary(tlsumm, self.curStep)
                if self.verboseOutput:
                    print ("t_l_: "+str(test_loss) )
                
                    
            
            if hasattr(data, "get_next_batch"):
                curData = data.get_next_batch()
    
            else:
                if curLoc < self.batchSize:
                    self.permutation = np.random.permutation(nSamples)
                
                curData = data[self.permutation[curLoc:(curLoc+self.batchSize)],:]
            
            if not self.log_everything :
                if  self.save_summary_every_n != 0 and self.curStep % self.save_summary_every_n == 0 :
                    summaries, loss_val, _ = self.sess.run([self.merged_summaries, self.loss,self.opt_op],
                                                           {self.data_in:curData, self.global_step:self.curStep}
                                                          )
                    self.writer.add_summary(summaries, self.curStep)
                else:
                     loss_val, _ = self.sess.run([self.loss,self.opt_op],
                                                 {self.data_in:curData, self.global_step:self.curStep} 
                                                )
            else:
                extraOutputs = self.network["midpoints"].values()+self.network["weights"]
                finalOutput = self.sess.run([self.merged_summaries, self.loss,self.opt_op]+extraOutputs,{self.data_in:curData, self.global_step:self.curStep} )
                summaries, loss_val, _ = finalOutput[0:3]
                self.writer.add_summary(summaries, self.curStep)
                    
                toBeSaved={}
                for tensorname,val in zip (self.network["midpoints"].keys()+[x.name for x in self.network["weights"] ]
                                           ,finalOutput[3:]):
                    toBeSaved[tensorname] = val
                
                np.savez(self.summaries_dir+"/arrayvalues_%03d"%self.curStep, **toBeSaved)
                #print (self.summaries_dir)
                
#                 np.savez(self.summaries_dir+"/arrayvalues_%03d"%self.curStep, *finalOutput[3:])
#                 with open(self.summaries_dir+"/arraynames_%03d.pkl"%self.curStep,'wb') as f:
#                     pickle.dump([x.name for x in extraOutputs],f)

            
            self.curStep += 1
            if (self.verboseOutput and i % 100 == 0):
                print ("loss: "+str(loss_val))
            
    def getStats(self,data):
        nSamples = data.shape[0]
        predictions = []
        for i in range(self.nStatIters):
            
            curLoc = (i*self.batchSize)%(nSamples-self.batchSize+1)
            if curLoc == 0:
                permutation = np.random.permutation(nSamples)
            curData = data[permutation[curLoc:(curLoc+self.batchSize)],:]
            predictions.append(self.sess.run([self.predictions],{self.data_in:curData} ) )
        all_predictions = np.sort( np.append([],predictions) )
        self.predictions_mean = np.mean(all_predictions)
        self.predictions_std = np.std(all_predictions)
        
    def predict(self, data):
        return self.decision_function(data)

    def decision_function(self, data):
        nSamples = data.shape[0]
        retval = np.zeros(nSamples)
        layer_to_use = self.predictions
#         layer_to_use = self.units_sum
        processedUpTo = 0
        for curLoc in range(0, nSamples - self.batchSize + 1, self.batchSize):
            processedUpTo = curLoc + self.batchSize
            curData = data[curLoc:processedUpTo, :]
            result = self.sess.run(layer_to_use, {self.data_in: curData})
            retval[curLoc:processedUpTo] = result
        if processedUpTo < nSamples:
            filled_pos = (nSamples - processedUpTo)
            curData = np.pad(data[processedUpTo:, :],
                             ((0, self.batchSize - filled_pos), (0, 0)),
                             'constant', constant_values=(0)
                             )
            result = self.sess.run(layer_to_use, {self.data_in: curData})
            retval[processedUpTo:] = result[:filled_pos]
            
        return -retval;
#         return -(retval - self.predictions_mean)/self.predictions_std

    def intermediate_values(self, data):
        return self.sess.run([self.xconcatedWithOnes,self.x_normed, self.afterMul, self.units_sum,
                                                                                   self.extra_unit_added,
                                                                                   self.cross_entropy,
                                                                                   self.loss
                                                                                   ], {self.data_in: data})
