'''
Created on Nov 30, 2016
Single Layer on single mode data.
@author: amir
'''
from OutDet import NetworkBuilder2
from OutDet import NetworkTrainer
from OutDet import OriginGenerators

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
import modelTestsSingleLayer.E0_experiment_helper as helper

clf = NetworkTrainer.NetworkOutlierDetector(
                        NetworkBuilder2.NetworkBuilder(
                            nInternalUnits = 100,
                            weight_reg_scale = 0.00001,
                            biases_reg_scale = 0.00001,
                            weight_init_stddev = 0.1,
                            bias_initializer = 0.0,
                            softmaxBased = True,
                            bias_lr_mult = 1.0,
                            std_normalize = False,
                            #seperation_points_gen=OriginGenerators.Aggregator(1,1.0)
                            ),
                        nIterations = 100,
                        learning_rate = 0.001,
                        log_everything_as_nparray = False,
                        justInit_Dont_train = True
                        )
helper.testModel(clf,
                 helper.single_lump_generator(0.2),
                 num_iterations = 100,
                 drawDir = "res/soft8/",
#                  plotLimSize = 2,   
#                  plotLimGridSize=50,
                 )


# clf = svm.OneClassSVM(kernel='linear', nu=0.1)
# helper.testModel(clf,helper.single_lump_generator(0.2), num_iterations = 1,drawDir = "res/ocsvm/",)




    

