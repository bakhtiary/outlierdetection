'''
Created on Dec 12, 2016

@author: amir
'''

import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
from matplotlib.pyplot import draw

def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)


class single_lump_generator():
    def __init__(self, loc = 0):
        self.loc = loc;
    def __call__(self):
    
        # Generate train data
        X = 0.3 * np.random.randn(100, 2)
        X_train = np.r_[X+self.loc]
        # Generate some regular novel observations
        X = 0.3 * np.random.randn(20, 2)
        X_test = np.r_[X+self.loc ]
        # Generate some abnormal novel observations
        X_outliers = np.random.uniform(low=-4, high=4, size=(20, 2))
        
        return X_train, X_test, X_outliers

def double_lump_generator():
    # Generate train data
    X1 = 0.3 * np.random.randn(500, 2)
    X2 = 0.3 * np.random.randn(500, 2)
    X_train = np.r_[X1+2, X2-2]
    # Generate some regular novel observations
    X = 0.3 * np.random.randn(20, 2)
    X_test = np.r_[X , X ]
    # Generate some abnormal novel observations
    X_outliers = np.random.uniform(low=-4, high=4, size=(20, 2))
    
    return X_train, X_test, X_outliers

def triple_lump_generator():
    # Generate train data
    X1 = 0.3 * np.random.randn(34, 2)
    X2 = 0.3 * np.random.randn(33, 2)
    X3 = 0.3 * np.random.randn(33, 2)
    
    X_train = np.r_[X1+3, X2, X1-3]
    # Generate some regular novel observations
    X = 0.3 * np.random.randn(20, 2)
    X_test = np.r_[X , X ]
    # Generate some abnormal novel observations
    X_outliers = np.random.uniform(low=-4, high=4, size=(20, 2))
    
    return X_train, X_test, X_outliers


def testModel (clf,data_generator_function, num_iterations = 10,drawDir = "tmp/",plotLimSize=4,plotLimGridSize=500):
    ensure_dir(drawDir)
    for i in range (num_iterations):
        
    
        X_train, X_test, X_outliers = data_generator_function()
        
        # fit the model
        if i == 0:
            clf.fit(X_train)
        else:
            clf.train(X_train)
        y_pred_train = clf.predict(X_train)
        y_pred_test = clf.predict(X_test)
        y_pred_outliers = clf.predict(X_outliers)
        n_error_train = y_pred_train[y_pred_train == -1].size
        n_error_test = y_pred_test[y_pred_test == -1].size
        n_error_outliers = y_pred_outliers[y_pred_outliers == 1].size
    
        # plot the line, the points, and the nearest vectors to the plane
        xx, yy = np.meshgrid(np.linspace(-plotLimSize, plotLimSize, plotLimGridSize), np.linspace(-plotLimSize, plotLimSize, plotLimGridSize))
        Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
     
        plt.title("Novelty Detection")
        plt.contourf(xx, yy, Z, levels=np.linspace(Z.min(), Z.max()+0.000001, 20), cmap=plt.cm.PuBu)
        plt.colorbar()
        a = plt.contour(xx, yy, Z, levels=[0], linewidths=2, colors='darkred')
#        plt.contourf(xx, yy, Z, levels=[0, Z.max()], colors='palevioletred')
    
        s = 40
        b1 = plt.scatter(X_train[:, 0], X_train[:, 1], c='white', s=s)
        b2 = plt.scatter(X_test[:, 0], X_test[:, 1], c='blueviolet', s=s)
        c = plt.scatter(X_outliers[:, 0], X_outliers[:, 1], c='gold', s=s)
        plt.axis('tight')
        plt.xlim((-plotLimSize, plotLimSize))
        plt.ylim((-plotLimSize, plotLimSize))
#         plt.legend([a.collections[0], b1, b2, c],
#                    ["learned frontier", "training observations",
#                     "new regular observations", "new abnormal observations"],
#                    loc="upper left",
#                    prop=matplotlib.font_manager.FontProperties(size=11))
        plt.xlabel(
            "error train: %d/200 ; errors novel regular: %d/40 ; "
            "errors novel abnormal: %d/40"
            % (n_error_train, n_error_test, n_error_outliers)
            )
        
        plt.savefig("%s%03d.png"%(drawDir,i))
    #    plt.show()
        plt.close()
