'''
Created on Nov 30, 2016
Single Layer on single mode data.
@author: amir
'''
from OutDet import NetworkBuilder2, OriginGenerators
from OutDet import NetworkTrainer

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
import modelTestsSingleLayer.E0_experiment_helper as helper

# clf = svm.OneClassSVM(kernel='linear', nu=0.1)
# helper.testModel(clf,helper.single_lump_generator(1.0), num_iterations = 1,drawDir = "res/ocsvm3/")

class normal_generator(object):
    '''
    classdocs
    '''


    def __init__(self, scale=1, randGen=np.random.RandomState(12345678)):
        '''
        Constructor
        '''
        self.scale = scale
        self.randGen = randGen
        
            
    def generate(self,data,nUnits,standardize_if_working_with_iterator = False):
        feat_dim = data.shape[1]
        return self.randGen.normal(scale=self.scale,size=[nUnits,feat_dim]) 
         
    
    def __repr__(self):
        return "gaus_%s"%(self.scale)   
    
    def __str__(self):
        return "gaus_%s"%(self.scale)

clf = NetworkTrainer.NetworkOutlierDetector(
                        NetworkBuilder2.NetworkBuilder(
                            nInternalUnits = 100,
                            weight_reg_scale = 1.0,
                            biases_reg_scale = 1.0,
                            weight_init_stddev = 0.1,
                            bias_initializer = 0.0,
                            softmaxBased = True,
                            bias_lr_mult = 1.0,
                            std_normalize = False,
#                             seperation_points_gen=OriginGenerators.Aggregator(1,1.0)
                              seperation_points_gen = normal_generator(3.0),
                            ),
                        summaries_dir="../../tf_log/", save_summary_every_n=10,
                        nIterations = 100,
                        learning_rate = 0.001,
                        log_everything_as_nparray = False,
                        justInit_Dont_train = True,
                        lr_decay_steps = 5000,
                        
                        )
helper.testModel(clf,
             helper.single_lump_generator(1.0),
                 num_iterations = 100,
                 drawDir = "../../res/linmpsvm_100Units_randomOrigin_HigherReg/",
#                  plotLimSize = 2,   
#                  plotLimGridSize=50,
                 )



    

