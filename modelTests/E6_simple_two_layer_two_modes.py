'''
Created on Nov 30, 2016
Single Layer on single mode data.
@author: amir
'''

from OutDet import NetworkBuilder
from OutDet import NetworkTrainer

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
import modelTests.E0_experiment_helper as helper

      

# clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
# clf2 = EllipticEnvelope(contamination=0.1)
clf3 = NetworkTrainer.NetworkOutlierDetector(NetworkBuilder.SimpleBuilder_TwoLayers(20),log_everything = True)
helper.testModel(clf3,helper.double_lump_generator)

    

    