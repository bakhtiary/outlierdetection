'''
Created on Nov 30, 2016
Single Layer on single mode data.
@author: amir
'''

from OutDet import NetworkBuilder
from OutDet import NetworkTrainer

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
import modelTests.E0_experiment_helper as helper

clf3 = NetworkTrainer.NetworkOutlierDetector(NetworkBuilder.SimpleBuilder(10), nIterations = 10,learning_rate = 0.0001, log_everything_as_nparray = False)
helper.testModel(clf3,helper.double_lump_generator, num_iterations = 100)



    
    
    

