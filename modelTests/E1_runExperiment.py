'''
Created on Nov 30, 2016
Single Layer on single mode data.
@author: amir
'''
from OutDet import NetworkBuilder
from OutDet import NetworkTrainer

from sklearn import svm
from sklearn.covariance import EllipticEnvelope
import modelTests.E0_experiment_helper as helper
from tensorflow.python.platform.tf_logging import log_every_n

clf3 = NetworkTrainer.NetworkOutlierDetector(NetworkBuilder.SimpleBuilder(100), nIterations = 100,learning_rate = 0.001, log_everything_as_nparray = False)
helper.testModel(clf3,helper.single_lump_generator(2), num_iterations = 100)


    

