'''
Created on Nov 30, 2016
Single Layer on single mode data.
@author: amir
'''

from OutDet import NetworkBuilder
from OutDet import NetworkTrainer

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
import modelTests.E0_experiment_helper as helper


clf3 = NetworkTrainer.NetworkOutlierDetector(NetworkBuilder.SimpleBuilder_AddAOne(100), nIterations = 100,learning_rate = 0.0001, log_everything = True)
helper.testModel(clf3,helper.double_lump_generator, num_iterations = 100)



 
    
    

